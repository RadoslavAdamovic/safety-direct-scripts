//This is function which we will use on the /checkout/cart/ page
  function extractCartItemInfo(cartItemElement) {
    const itemInfo = {};
    const itemNameElement = cartItemElement.querySelector('.product-item-name a');
    itemInfo.item_name = itemNameElement ? itemNameElement.innerText.trim() : '';
    const itemBrandElement = cartItemElement.querySelector('.brand_image img');
    itemInfo.item_brand = itemBrandElement ? itemBrandElement.getAttribute('alt') : '';
    const skuElement = cartItemElement.querySelector('.sku');
    itemInfo.item_sku = skuElement ? skuElement.innerText.trim().replace('SKU: ', '') : '';
    const quantityInput = cartItemElement.querySelector('.control.qty input');
    itemInfo.item_quantity = quantityInput ? parseInt(quantityInput.value) : 0;
    const priceIncludingTaxElement = cartItemElement.querySelector('.price-including-tax .price');
    itemInfo.price_with_tax = priceIncludingTaxElement ? parseFloat(priceIncludingTaxElement.innerText.replace('€', '').trim()) : 0;
    const priceExcludingTaxElement = cartItemElement.querySelector('.price-excluding-tax .price');
    itemInfo.price_without_tax = priceExcludingTaxElement ? parseFloat(priceExcludingTaxElement.innerText.replace('€', '').trim()) : 0;
    const itemOptionsList = cartItemElement.querySelectorAll('.item-options .item-row');
    if (itemOptionsList) {
      itemInfo.item_attributes = [];
      itemOptionsList.forEach(itemOption => {
        const label = itemOption.querySelector('dt').innerText.trim();
        const value = itemOption.querySelector('dd').innerText.trim();
        itemInfo.item_attributes.push(`${label}: ${value}`);
      });
    } else {
      itemInfo.item_attributes = [];
    }
    
    return itemInfo;
  }
const removeButtons = document.querySelectorAll('.action-delete');
removeButtons.forEach(button => {
  button.addEventListener('click', function(event) {
    event.preventDefault();
    const itemRow = button.closest('.item-info');
    const itemInfo = extractCartItemInfo(itemRow);
    console.log('Item to be removed:', itemInfo);
    });
});

