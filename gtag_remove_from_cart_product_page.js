// Function to extract item info
function extractItemInfo(itemElement) {
  const itemInfo = {};
  const itemNameElement = itemElement.querySelector('.product-item-name a');
  itemInfo.item_name = itemNameElement ? itemNameElement.innerText.trim() : '';
  const attributesList = itemElement.querySelectorAll('.product.options.list div');
  if (attributesList.length > 0) {
      itemInfo.item_attributes = [];
      attributesList.forEach(attribute => {
          const label = attribute.querySelector('dt.label').innerText.trim();
          const value = attribute.querySelector('dd.values span').innerText.trim();
          itemInfo.item_attributes.push(`${label}: ${value}`);
      });
  } else {
      itemInfo.item_attributes = [];
  }
  const priceIncludingTaxElement = itemElement.querySelector('.price-including-tax .price');
  const priceExcludingTaxElement = itemElement.querySelector('.price-excluding-tax .price');
  itemInfo.item_price_with_tax = priceIncludingTaxElement ? parseFloat(priceIncludingTaxElement.innerText.replace('€', '').trim()) : 0;
  itemInfo.item_price_without_tax = priceExcludingTaxElement ? parseFloat(priceExcludingTaxElement.innerText.replace('€', '').trim()) : 0;
  const quantityInput = itemElement.querySelector('.details-qty.qty .item-qty.cart-item-qty');
  itemInfo.item_quantity = quantityInput ? parseInt(quantityInput.getAttribute('data-item-qty')) : 0;
  const itemId = quantityInput ? quantityInput.getAttribute('data-cart-item') : '';
  const itemSku = quantityInput ? quantityInput.getAttribute('data-cart-item-id') : '';
  itemInfo.item_id = itemId;
  itemInfo.item_sku = itemSku;
  return itemInfo;
}
function extractAllItemsInfo() {
  const container = document.getElementById('minicart-content-wrapper');
  const itemElements = container.querySelectorAll('.item.product.product-item');
  const itemsInfo = [];

  itemElements.forEach(itemElement => {
    const itemInfo = extractItemInfo(itemElement);
    itemsInfo.push(itemInfo);
  });
  return itemsInfo;
}
let ItemsBeforeMutation = [];
function handleItemRemoval(removedItem) {
  // Extract information about the removed item
  const removedItemInfo = extractItemInfo(removedItem);
  ItemsBeforeMutation.push(removedItemInfo);
}
function compareItemsBeforeAndAfterMutation() {
  let ItemsInfo = extractAllItemsInfo();
  ItemsBeforeMutation.forEach(itemBefore => {
    const found = ItemsInfo.find(itemAfter => itemAfter.item_id === itemBefore.item_id);
    if (!found) {
      console.log('Item removed:', itemBefore);
      return;
    }
  });
  ItemsBeforeMutation = [];
      ItemsInfo = [];
}
const minicartContentWrapper = document.getElementById('minicart-content-wrapper');
const observer = new MutationObserver(mutationsList => {
  mutationsList.forEach(mutation => {
    if (mutation.removedNodes.length > 0) {
      mutation.removedNodes.forEach(removedNode => {
        if (removedNode.classList && removedNode.classList.contains('item')) {
          handleItemRemoval(removedNode);
        }
      });
    }
  });
  compareItemsBeforeAndAfterMutation();
});
const config = { childList: true, subtree: true };
observer.observe(minicartContentWrapper, config);
