function extractDataFromPageGrid() {
  const productRows = document.querySelectorAll('tbody > tr');
  const productsData = [];

  productRows.forEach(row => {
      const quantity = parseInt(row.querySelector('.qty').value);
      if (quantity > 0) {
          const productId = row.querySelector('.productDetails input').value;
          const productName = row.querySelector('.productDetails .product-name').textContent.trim();
          const sku = row.querySelector('.sku span').textContent.trim();
          const priceExVat = row.querySelector('.exVat span').textContent.trim();
          const priceIncVat = row.querySelector('.incVat span').textContent.trim();
          const attributeCells = row.querySelectorAll('td:nth-child(n+3):not(.prices)');
          const itemAttributes = [];
          attributeCells.forEach(cell => {
              const attributeParagraph = cell.querySelector('p');
              if (attributeParagraph) {
                  const attributeValue = attributeParagraph.textContent.trim();
                  itemAttributes.push({ attributeValue });
              }
          });

          productsData.push({
              item_id: productId,
              item_name: productName,
              item_sku: sku,
              price_with_tax: priceExVat,
              price_without_tax: priceIncVat,
              item_quantity: quantity,
              item_attributes: itemAttributes
          });
      }
  });

  console.log(productsData);
}
  const addToBasketButton = document.querySelector('tfoot .tocart.primary');
  addToBasketButton.addEventListener('click', function() {
      extractDataFromPageGrid();
  });

function extractDataFromPage() {
  const productElement = document.querySelector('.column.main');
      const item_id = productElement.querySelector('.price-box').getAttribute('data-product-id');
      const item_name = productElement.querySelector('.page-title-wrapper.product .base').innerText.trim();
      const item_brand = productElement.querySelector('.brand_image img').getAttribute('alt');
      const item_sku = productElement.querySelector('.product.attribute.sku .value[itemprop="sku"]').innerText;
      const quantityInput = productElement.querySelector('input[name="qty"]');
      const item_quantity = parseInt(quantityInput.value);
      let item_lens_color = 'Not specified'; 
      const selectElement757 = productElement.querySelector('#attribute757');
      if (selectElement757) {
        const selectedOption757 = selectElement757.options[selectElement757.selectedIndex];
        item_lens_color = selectedOption757.innerText.trim();
      }
      let item_unit = productElement.querySelector('.product.attribute.unit .value[itemprop="unit"]').innerText;
      const selectElement903 = productElement.querySelector('#attribute903');
      if (selectElement903) {
        const selectedOption903 = selectElement903.options[selectElement903.selectedIndex];
        item_unit = selectedOption903.innerText.trim();
      }
      let item_frame_type = 'Not specified';
      const selectElement1103 = productElement.querySelector('#attribute1103');
      if (selectElement1103) {
        const selectedOption1103 = selectElement1103.options[selectElement1103.selectedIndex];
        item_frame_type = selectedOption1103.innerText.trim();
      }
      let item_frame_material = 'Not specified';
      const selectElement1105 = productElement.querySelector('#attribute1105');
      if (selectElement1105) {
        const selectedOption1105 = selectElement1105.options[selectElement1105.selectedIndex];
        item_frame_material = selectedOption1105.innerText.trim();
      }
      let item_colour = 'Not specified';
      let selectElement664 = productElement.querySelector('.swatch-attribute.colour');
      if (selectElement664) {
        item_colour = selectElement664.querySelector('.swatch-attribute-selected-option').innerText.trim();
      }
      const price_with_tax = parseFloat(productElement.querySelector('.price-including-tax .price').innerText.replace('€', '').trim());
      const price_without_tax = parseFloat(productElement.querySelector('.price-excluding-tax .price').innerText.replace('€', '').trim());
      const categoriesList = productElement.querySelectorAll('.product-category-tags ul li a');
      const item_categories = [];
      categoriesList.forEach(category => {
        item_categories.push(category.innerText.trim());
      });
      const attributesList = productElement.querySelectorAll('.product.attribute.overview ul li');
      const item_attributes = [];
      attributesList.forEach(attribute => {
        item_attributes.push(attribute.innerText.trim());
      });
      if (((item_lens_color !== 'Not specified' && item_lens_color !== 'Choose an Option...') && selectElement757) ||
    ((item_unit !== 'Not specified' && item_unit !== 'Choose an Option...') && selectElement903) ||
    ((item_frame_type !== 'Not specified' && item_frame_type !== 'Choose an Option...') && selectElement1103) ||
    ((item_frame_material !== 'Not specified' && item_frame_material !== 'Choose an Option...') && selectElement1105) ||
    ((item_colour !== 'Not specified' && item_colour !== '') && selectElement664)) {
          }
          else if (!(selectElement757 || selectElement903 || selectElement1103 || selectElement1105 || selectElement664)) {
        } else {
       console.log("Skipping data extraction. User need to fulfill a required field.");
       return;
      }
      const productData = {
          item_id,
          item_name,
          item_brand,
          item_sku,
          item_unit,
          item_lens_color,
          item_frame_type,
          item_frame_material,
          item_colour,
          price_with_tax,
          price_without_tax,
          item_categories,
          item_attributes,
          item_quantity
      };
      console.log(productData);
}
const addToBasketButtonSimple = document.getElementById('product-addtocart-button');
addToBasketButtonSimple.addEventListener('click', function(event) {
        extractDataFromPage();
});