function extractCartItemInfo(cartItemElement) {
  const itemInfo = {};
  const itemNameElement = cartItemElement.querySelector('.product-item-name');
  itemInfo.item_name = itemNameElement ? itemNameElement.innerText.trim() : '';
  const quantityInput = cartItemElement.querySelector('.details-qty .value');
  itemInfo.item_quantity = quantityInput.innerText.trim();
  const priceIncludingTaxElement = cartItemElement.querySelector('.price-including-tax .price');
  itemInfo.price_with_tax = priceIncludingTaxElement ? parseFloat(priceIncludingTaxElement.innerText.replace('€', '').trim()) : 0;
  const priceExcludingTaxElement = cartItemElement.querySelector('.price-excluding-tax .price');
  itemInfo.price_without_tax = priceExcludingTaxElement ? parseFloat(priceExcludingTaxElement.innerText.replace('€', '').trim()) : 0;
  const itemOptionsList = cartItemElement.querySelectorAll('.item-options');
  if (itemOptionsList) {
    itemInfo.item_attributes = [];
    itemOptionsList.forEach(itemOption => {
      const label = itemOption.querySelector('dt').innerText.trim();
      const value = itemOption.querySelector('dd').innerText.trim();
      itemInfo.item_attributes.push(`${label}: ${value}`);
    });
  } else {
    itemInfo.item_attributes = [];
  }
  
  return itemInfo;
}

const cartItemElements = document.querySelectorAll('.product-item');
const cartItems = [];
cartItemElements.forEach(cartItemElement => {
  const itemInfo = extractCartItemInfo(cartItemElement);
  cartItems.push(itemInfo);
});
const shippingMethod = document.querySelector('.totals.shipping.incl .value').textContent.trim();
const shippingCost = document.querySelector('.totals.shipping.incl .price').textContent.trim();
const total = document.querySelector('.grand.totals .price').textContent.trim();
const currency = 'EUR';
console.log('Shipping Method:', shippingMethod);
console.log('Shipping Cost:', shippingCost);
console.log('Total:', total);
console.log('Currency:', currency);
console.log(cartItems);

