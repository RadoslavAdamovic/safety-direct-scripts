
items: [
  {
    item_id: "58249",
    item_name: "Uvex Pheos CX2 Safety Glasses",
    index: 0,
    item_brand: "Uvex",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Glasses",
    item_category4: "Uvex",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "default",
    location_id: "/",
    price_with_tax: 10.70,
    price_without_tax: 8.70,
    quantity: 1
  },
  {
    item_id: "59939",
    item_name: "Bolle Lens Cleaning Station",
    index: 1, 
    item_brand: "Bolle",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Lens Cleaning & Accessories",
    item_category4: "Bolle",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "default", 
    location_id: "/", 
    price_with_tax: 88.07, 
    price_without_tax: 71.60, 
    quantity: 1 
  },
  {
    item_id: "60027",
    item_name: "Bolle Cobra Hybrid Safety Glasses",
    index: 2, 
    item_brand: "Bolle",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Glasses",
    item_category4: "Bolle",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "default", 
    location_id: "/", 
    price_with_tax: 19.19, 
    price_without_tax: 15.60, 
    quantity: 1 
  },
  {
    item_id: "60030",
    item_name: "Bolle Cobra Safety Glasses",
    index: 3, 
    item_brand: "Bolle",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Glasses",
    item_category4: "Bolle",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "default", 
    location_id: "/", 
    price_with_tax: 14.08, 
    price_without_tax: 11.45, 
    quantity: 1 
  },
  {
    item_id: "60545",
    item_name: "Bolle Lens Wipes Dispensers",
    index: 4, 
    item_brand: "Bolle",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Lens Cleaning & Accessories",
    item_category4: "Bolle",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "default", 
    location_id: "/", 
    price_with_tax: 12.79, 
    price_without_tax: 10.40, 
    quantity: 1 
  },
  {
    item_id: "60797",
    item_name: "Bolle IRI-S Safety Glasses",
    index: 5, 
    item_brand: "Bolle",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Glasses",
    item_category4: "Bolle",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "default", 
    location_id: "/", 
    price_with_tax: 16.91, 
    price_without_tax: 13.75, 
    quantity: 1
  },
  {
    item_id: "61260",
    item_name: "Bolle Ness Plus Safety Glasses",
    index: 6, 
    item_brand: "Bolle",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Bolle",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "default", 
    location_id: "/", 
    price_with_tax: 13.04, 
    price_without_tax: 10.60, 
    quantity: 1 
  },
  {
    item_id: "61324",
    item_name: "Bolle Rush Plus Safety Glasses",
    index: 7, 
    item_brand: "Bolle",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Safety Glasses",
    item_category4: "Bolle",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "default", 
    location_id: "/", 
    price_with_tax: 12.30, 
    price_without_tax: 10.00, 
    quantity: 1 
  },  
  {
    item_id: "61591",
    item_name: "Bolle Bandido Clear Safety Glasses",
    index: 8, 
    item_brand: "Bolle",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Glasses",
    item_category4: "Bolle",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "default", 
    location_id: "/", 
    price_with_tax: 6.40, 
    price_without_tax: 5.20, 
    quantity: 1 
  },
  {
    item_id: "23182",
    item_name: "Uvex Sportstyle Safety Glasses Clear",
    index: 9, 
    item_brand: "Uvex",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Safety Glasses",
    item_category4: "Uvex",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "default", 
    location_id: "/", 
    price_with_tax: 9.96, 
    price_without_tax: 8.10, 
    quantity: 1 
  },
  {
    item_id: "25536",
    item_name: "Pelsafe Visitor Glasses",
    index: 10,
    item_brand: "Pelsafe",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Glasses",
    item_category4: "Pelsafe",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "",
    location_id: "/",
    price_with_tax: 3.14,
    price_without_tax: 2.55,
    quantity: 1
  },
  {
    item_id: "25535",
    item_name: "Pelsafe Economy Safety Goggles",
    index: 11,
    item_brand: "Pelsafe",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Goggles",
    item_category4: "Pelsafe",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "",
    location_id: "/",
    price_with_tax: 3.57,
    price_without_tax: 2.90,
    quantity: 1
  },
  {
    item_id: "22694",
    item_name: "Uvex I-Works Safety Glasses",
    index: 12,
    item_brand: "Uvex",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Glasses",
    item_category4: "Uvex",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "",
    location_id: "/",
    price_with_tax: 8.36,
    price_without_tax: 6.80,
    quantity: 1
  },
  {
    item_id: "20104",
    item_name: "3M SecureFit Safety Glasses",
    index: 13,
    item_brand: "3M",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Glasses",
    item_category4: "3M",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "",
    location_id: "/",
    price_with_tax: 11.56,
    price_without_tax: 9.40,
    quantity: 1
  },
  {
    item_id: "28649",
    item_name: "Uvex Visitor Over Spec Glasses",
    index: 14,
    item_brand: "Uvex",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Glasses",
    item_category4: "Uvex",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "",
    location_id: "/",
    price_with_tax: 5.10,
    price_without_tax: 4.15,
    quantity: 1
  },
  {
    item_id: "26452",
    item_name: "Uvex Super OTG Visitors Glasses",
    index: 15,
    item_brand: "Uvex",
    item_category: "Personal Protection",
    item_category2: "Eye Protection",
    item_category3: "Glasses",
    item_category4: "Uvex",
    item_list_id: "related_products",
    item_list_name: "Related Products",
    item_variant: "",
    location_id: "/",
    price_with_tax: 12.73,
    price_without_tax: 10.35,
    quantity: 1
  },
  
]
