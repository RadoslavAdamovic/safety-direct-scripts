//This is function for view_item event which will be trigger when user enters the product page
function extractDataFromPage() {
  const productElement = document.querySelector('.column.main');
      const item_id = productElement.querySelector('.price-box').getAttribute('data-product-id');
      const item_name = productElement.querySelector('.page-title-wrapper.product .base').innerText.trim();
      const item_brand = productElement.querySelector('.brand_image img').getAttribute('alt');
      let item_lens_color = 'Not specified'; 
      const item_sku = productElement.querySelector('.product.attribute.sku .value[itemprop="sku"]').innerText;
      const selectElement757 = productElement.querySelector('#attribute757');
      if (selectElement757) {
        const selectedOption757 = selectElement757.options[selectElement757.selectedIndex];
        item_lens_color = selectedOption757.innerText.trim();
      }
      let item_unit = productElement.querySelector('.product.attribute.unit .value[itemprop="unit"]').innerText;
      const selectElement903 = productElement.querySelector('#attribute903');
      if (selectElement903) {
        const selectedOption903 = selectElement903.options[selectElement903.selectedIndex];
        item_unit = selectedOption903.innerText.trim();
      }
      let item_frame_type = 'Not specified';
      const selectElement1103 = productElement.querySelector('#attribute1103');
      if (selectElement1103) {
        const selectedOption1103 = selectElement1103.options[selectElement1103.selectedIndex];
        item_frame_type = selectedOption1103.innerText.trim();
      }
      let item_frame_material = 'Not specified';
      const selectElement1105 = productElement.querySelector('#attribute1105');
      if (selectElement1105) {
        const selectedOption1105 = selectElement1105.options[selectElement1105.selectedIndex];
        item_frame_material = selectedOption1105.innerText.trim();
      }
      let item_colour = 'Not specified';
      let selectElement664 = productElement.querySelector('.swatch-attribute.colour');
      if (selectElement664) {
        item_colour = selectElement664.querySelector('.swatch-attribute-selected-option').innerText.trim();
      }
      const price_with_tax = parseFloat(productElement.querySelector('.price-including-tax .price').innerText.replace('€', '').trim());
      const price_without_tax = parseFloat(productElement.querySelector('.price-excluding-tax .price').innerText.replace('€', '').trim());
      const categoriesList = productElement.querySelectorAll('.product-category-tags ul li a');
      const item_categories = [];
      categoriesList.forEach(category => {
        item_categories.push(category.innerText.trim());
      });
      const attributesList = productElement.querySelectorAll('.product.attribute.overview ul li');
      const item_attributes = [];
      attributesList.forEach(attribute => {
        item_attributes.push(attribute.innerText.trim());
      });
      const productData = {
          item_id,
          item_name,
          item_brand,
          item_sku,
          item_unit,
          item_lens_color,
          item_frame_type,
          item_frame_material,
          item_colour,
          price_with_tax,
          price_without_tax,
          item_categories,
          item_attributes
      };
      console.log(productData);
}
const selectElement757 = document.querySelector('#attribute757');
const selectElement903 = document.querySelector('#attribute903');
const selectElement1103 = document.querySelector('#attribute1103');
const selectElement1105 = document.querySelector('#attribute1105');
const selectElement664 = document.querySelectorAll('.swatch-option.color');
if (selectElement757 || selectElement903 || selectElement1103 || selectElement1105 || selectElement664 ) {
  if (selectElement757) {
    selectElement757.addEventListener('change', extractDataFromPage);
  }
  if (selectElement903) {
    selectElement903.addEventListener('change', extractDataFromPage);
  }
  if (selectElement1103) {
    selectElement1103.addEventListener('change', extractDataFromPage);
  }
  if (selectElement1105) {
    selectElement1105.addEventListener('change', extractDataFromPage);
  }
  if (selectElement664) {
    selectElement664.forEach(element => {
      element.addEventListener('click',() => {
        setTimeout(() => {
          extractDataFromPage(); 
        }, 100);
      });
    });
  }
} else {
  extractDataFromPage();
}

