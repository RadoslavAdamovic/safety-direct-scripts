function extractDataAndProcess() {
  const productElements = document.querySelectorAll('.item.product.product-item');
  const productsArray = [];
  productElements.forEach((productElement, index) => {
      const item_id = productElement.querySelector('.price-box').getAttribute('data-product-id');
      const item_name = productElement.querySelector('.product-item-name a').innerText.trim();
      const item_brand = productElement.querySelector('.brand_image img').getAttribute('alt');
      const price_with_tax = parseFloat(productElement.querySelector('.price-including-tax .price').innerText.replace('€', '').trim());
      const price_without_tax = parseFloat(productElement.querySelector('.price-excluding-tax .price').innerText.replace('€', '').trim());
      const productData = {
          item_id,
          item_name,
          index,
          item_brand,
          price_with_tax,
          price_without_tax,
      };
      productsArray.push(productData);
  });
  return productsArray; 
}

document.addEventListener('DOMContentLoaded', function() {
  const productsData = extractDataAndProcess(); 
  const eventData = {
    item_list_id: "related_products",
    item_list_name: "Related products",
    items: productsData 
  };
    gtag("event", "view_item_list", eventData);
});
