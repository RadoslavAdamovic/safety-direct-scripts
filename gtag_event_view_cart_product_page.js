//This is function which we will use on any product page

function extractItemInfo(itemElement) {
  const itemInfo = {};
  const itemNameElement = itemElement.querySelector('.product-item-name a');
  itemInfo.item_name = itemNameElement ? itemNameElement.innerText.trim() : '';
  const attributesList = itemElement.querySelectorAll('.product.options.list div');
  if (attributesList.length > 0) {
      itemInfo.item_attributes = [];
      attributesList.forEach(attribute => {
          const label = attribute.querySelector('dt.label').innerText.trim();
          const value = attribute.querySelector('dd.values span').innerText.trim();
          itemInfo.item_attributes.push(`${label}: ${value}`);
      });
  } else {
      itemInfo.item_attributes = [];
  }
  const priceIncludingTaxElement = itemElement.querySelector('.price-including-tax .price');
  const priceExcludingTaxElement = itemElement.querySelector('.price-excluding-tax .price');
  itemInfo.item_price_with_tax = priceIncludingTaxElement ? parseFloat(priceIncludingTaxElement.innerText.replace('€', '').trim()) : 0;
  itemInfo.item_price_without_tax = priceExcludingTaxElement ? parseFloat(priceExcludingTaxElement.innerText.replace('€', '').trim()) : 0;
  const quantityInput = itemElement.querySelector('.details-qty.qty .item-qty.cart-item-qty');
  itemInfo.item_quantity = quantityInput ? parseInt(quantityInput.getAttribute('data-item-qty')) : 0;
  const itemId = quantityInput ? quantityInput.getAttribute('data-cart-item') : '';
  const itemSku = quantityInput ? quantityInput.getAttribute('data-cart-item-id') : '';
  itemInfo.item_id = itemId;
  itemInfo.item_sku = itemSku;
  return itemInfo;
}
function extractCartInfo() {
  const container = document.querySelector('.minicart-wrapper');
  const subtotalElement = container.querySelector('.total-price .price');
  const subtotal_ex_vat = subtotalElement ? parseFloat(subtotalElement.innerText.replace('€', '').trim()) : 0;
  const summaryCountElement = container.querySelector('.counter.qty .counter-number');
  const number_of_items = summaryCountElement ? parseInt(summaryCountElement.innerText.trim()) : 0;

  return { subtotal_ex_vat, number_of_items };
}
function extractAllItemsInfo() {
  const container = document.getElementById('minicart-content-wrapper');
  const itemElements = container.querySelectorAll('.item.product.product-item');
  const itemsInfo = [];

  itemElements.forEach(itemElement => {
    const itemInfo = extractItemInfo(itemElement);
    itemsInfo.push(itemInfo);
  });

  return itemsInfo;
}
const cartTriggerLink = document.querySelector('.action.cart-trigger');
cartTriggerLink.addEventListener('click', function(event) {
  if (cartTriggerLink.classList.contains('action') &&
      cartTriggerLink.classList.contains('cart-trigger') &&
      cartTriggerLink.classList.contains('active')) {
  const { subtotal_ex_vat, number_of_items } = extractCartInfo();
  console.log('Subtotal (ex VAT):', subtotal_ex_vat);
  console.log('Number of items:', number_of_items);
  const itemsInfo = extractAllItemsInfo();
  console.log('Items:', itemsInfo);
      }
});
const updateButtons = document.querySelectorAll('.update-cart-item');
updateButtons.forEach(button => {
  button.addEventListener('click', function(event) {
    setTimeout(() => {
      const { subtotal_ex_vat, number_of_items } = extractCartInfo();
      console.log('Subtotal (ex VAT):', subtotal_ex_vat);
      console.log('Number of items:', number_of_items);
      const itemsInfo = extractAllItemsInfo();
      console.log('Items:', itemsInfo);
    }, 1000); 
  });
});
